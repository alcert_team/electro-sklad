<?php

use yii\db\Migration;

/**
 * Class m180321_104121_init_db
 */
class m180321_104121_init_db extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'description' => $this->string(),
            'key' => $this->string()->notNull()->unique(),
            'value' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('settings');
    }

//    // Use up()/down() to run migration code without a transaction.
//    public function up()
//    {
//
//    }
//
//    public function down()
//    {
//        echo "m180321_104121_init_db cannot be reverted.\n";
//
//        return false;
//    }

}
