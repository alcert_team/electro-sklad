<?php

use yii\db\Migration;

/**
 * Class m180402_124811_insert_base_recorcs
 */
class m180402_124811_insert_base_recorcs extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'id' => '1',
            'description' => 'Коэфициент',
            'key' => 'rate',
            'value' => '1.00',
        ]);
        $this->insert('settings', [
            'id' => '2',
            'description' => 'Телефон',
            'key' => 'phone',
            'value' => '+7 (999) 999-99-99',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('settings', ['id' => 1]);
        $this->delete('settings', ['id' => 2]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180402_124811_insert_base_recorcs cannot be reverted.\n";

        return false;
    }
    */
}
