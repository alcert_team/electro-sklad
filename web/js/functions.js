(function ($) {
    $("#logout").click(function (e) {
        e.preventDefault();
        $.ajax({
            url: "/settings/logout",
            type: "post",
            data: {},
            success: function (result) {
                $(location).attr('href', '/settings/login')
            },
            error: function (xhr, status, error) {
                // var err = eval("(" + xhr.responseText + ")");
                // alert(err.Message);
                $(location).attr('href', '/settings/')
            }
        });
    });
}(jQuery));