<?php
/**
 * Created by PhpStorm.
 * User: XIAOMIAIR
 * Date: 07.03.2018
 * Time: 8:57
 */

namespace app\alcert\exceptions;

use yii\web\ServerErrorHttpException;

class MaxFileSizeException extends ServerErrorHttpException
{
}