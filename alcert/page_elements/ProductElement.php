<?php
/**
 * Created by PhpStorm.
 * User: XIAOMIAIR
 * Date: 06.03.2018
 * Time: 10:19
 */

namespace app\alcert\page_elements;


class ProductElement extends ProductsElement
{
    private $infoTable;
    private $tabs;

    /**
     * @return mixed
     */
    public function getInfoTable()
    {
        return $this->infoTable;
    }

    /**
     * @param mixed $infoTable
     */
    public function setInfoTable($infoTable)
    {
        $this->infoTable = $infoTable;
    }

    /**
     * @return mixed
     */
    public function getTabs()
    {
        return $this->tabs;
    }

    /**
     * @param mixed $tabs
     */
    public function setTabs($tabs)
    {
        $this->tabs = $tabs;
    }

}