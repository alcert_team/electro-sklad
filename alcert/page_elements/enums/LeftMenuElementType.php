<?php

namespace app\alcert\page_elements\enums;

class LeftMenuElementType extends Enum
{
    const TEXT = 'TEXT';
    const LINK = 'LINK';

    public function getAll()
    {
        return [
            $this->TEXT,
            $this->LINK,
        ];
    }
}