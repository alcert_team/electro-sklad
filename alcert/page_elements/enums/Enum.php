<?php

namespace app\alcert\page_elements\enums;

use app\alcert\models\EnumException;
use yii\base\BaseObject;

abstract class Enum extends BaseObject
{
    private $current_val;

    /**
     * Enum constructor.
     * @param string $type
     * @throws EnumException
     */
    final public function __construct($type)
    {
        $class_name = get_class($this);

        $type = strtoupper($type);
        if (constant("{$class_name}::{$type}") === NULL) {
            throw new EnumException('Свойства ' . $type . ' в перечислении ' . $class_name . ' не найдено.');
        }

        $this->current_val = constant("{$class_name}::{$type}");

        parent::__construct();
    }

    final public function __toString()
    {
        return $this->current_val;
    }
}