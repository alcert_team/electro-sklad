<?php

namespace app\alcert\page_elements\plain;

use yii\base\BaseObject;

class TextElement extends BaseObject
{
    private $text;

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }


}