<?php
/**
 * @var yii\web\View $this
 */
use app\alcert\models\Settings;
use app\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$my_asset = AppAsset::register($this);
$this->beginPage() ?>
    <!doctype html>
    <html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <!--    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">-->

        <!-- Bootstrap CSS -->
        <?php $this->head() ?>

<!--        <title>--><?php //Html::encode($this->title) ?><!--</title></title>-->
        <title><?= Html::encode("ЭлектроСклад") ?></title>
    </head>
    <body>
    <?php $this->beginBody() ?>

    <div class="wrapper mx-auto">
        <div class="container">
            <div class="header-nav">
                <ul class="nav col-10 mx-auto font-weight-bold">
                    <li class="nav-item">
                        <a class="nav-link active" href="/about">О компании</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/catalog/">Продукция</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/dostavka">Доставка</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/warranty">Гарантия</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/contact">Контакты</a>
                    </li>
                </ul>
            </div>

            <div class="header row m-0 p-0">

                <div class="col-4 pl-5">
                    <img src="/img/logo4.png" width="250px" class="pt-4 pl-4">
                </div>

                <div class="col-8">

                    <div class="row">
                        <div class="col-8 text-center h4 pt-4 pr-5">
                            <div class="mt-2">
                                <strong>Подробная информация:</strong><br>
                                <?= Settings::findByKey("phone")->getValue(); ?>
                            </div>
                        </div>
                        <div class="col-4 font-weight-bold pt-4">
                            Мы работаем:<br>пн-чт с 9:00 до 18:00<br>пт с 9:00 до 17:00
                        </div>
                    </div>

                    <div class="search-field">
                        <?= Html::beginForm(['/search'], 'post', ['id' => 'search-form']) ?>
                        <?= Html::input("text", "search", "", ['class' => 'rounded h-50 w-75 pl-1', "placeholder" => "Поиск по сайту..."]) ?>
                        <?php Html::endForm() ?>
                    </div>

                </div>

            </div>

            <?php
            /**
             * @var array $sidebarExists
             * @var \app\alcert\page_elements\LeftMenuElement $leftMenuElement
             */
            $sidebarExists = isset($this->params['sidebar']) && !empty($this->params['sidebar']) && is_array($this->params['sidebar']);
            ?>

            <div class="content col-12 row mx-auto pt-4">
                <?php if ($sidebarExists) : ?>
                    <div class="sidebar col-3 m-0 p-0">
                        <div class="col-12 m-0 p-0">
                            <?php foreach ($this->params['sidebar'] as $leftMenuElement): ?>

                                <div class="px-2 py-1 rounded <?= $leftMenuElement->getClass() ?> mb-1">

                                    <?php $element = $leftMenuElement->getElement(); ?>
                                    <?php if ($element instanceof \app\alcert\page_elements\plain\LinkElement) : ?>
                                        <a href="<?= $element->getHref() ?>">
                                            <?= $leftMenuElement->getElement()->getText() ?>
                                        </a>
                                    <?php elseif ($element instanceof \app\alcert\page_elements\plain\TextElement) : ?>
                                        <span><?= $element->getText() ?></span>
                                    <?php endif; ?>

                                </div>

                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endif; ?>

                <div class="page-content col-<?= $sidebarExists ? "9" : "12" ?> m-0 p-0 pl-2">
                    <?=
                    /**
                     * @var string $content
                     */
                    $content
                    ?>
                </div>
            </div>
            <div class="footer">
                <div class="text-right mr-5">
                    2007 – <?= date("Y"); ?> © ЭлектроСклад
                </div>
            </div>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>