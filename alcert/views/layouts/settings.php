<?php
/**
 * @var yii\web\View $this
 */
use app\assets\AppAsset;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;

$my_asset = AppAsset::register($this);
$this->beginPage() ?>
    <!doctype html>
    <html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <!--    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">-->

        <link href='http://fonts.googleapis.com/css?family=Irish+Grover' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=La+Belle+Aurore' rel='stylesheet' type='text/css'>

        <!-- Bootstrap CSS -->
        <?php $this->head() ?>

        <title><?php Html::encode($this->title) ?></title></title>
    </head>
    <body>
    <?php $this->beginBody() ?>

    <div class="wrapper mx-auto">
        <div class="container">
            <div class="header-nav">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <a class="navbar-brand" href="#">ЭлектроСклад</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
                            aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarText">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="#">Настройки <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item active">
                                <?php if (Yii::$app->user->isGuest): ?>
                                    <a class="nav-link" href="/settings/login">Login <span
                                                class="sr-only">(current)</span></a>
                                <?php else: ?>
                                    <a id="logout" class="nav-link" href="#">
                                        <span>Logout(<?= Yii::$app->user->identity->username; ?>)</span>
                                    </a>
                                <?php endif; ?>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>

            <div class="content col-12 row mx-auto pt-4">
                <div class="page-content col-12 m-0 p-0">
                    <?=
                    /**
                     * @var string $content
                     */
                    $content
                    ?>
                </div>
            </div>

            <div class="footer"></div>
        </div>
    </div>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>