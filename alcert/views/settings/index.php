<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\alcert\models\SettingsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Settings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Settings', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'description',
            'key',
            'value:ntext',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a(
                            '<i class="fa fa-circle-thin" aria-hidden="true"></i>',
                            $url);
                    },
                    'update' => function ($url, $model) {
                        return Html::a(
                            '<i class="fa fa-edit" aria-hidden="true"></i>',
                            $url);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a(
                            '<i class="fa fa-remove" aria-hidden="true"></i>',
                            $url);
                    },
//                    'link' => function ($url,$model,$key) {
//                        return Html::a('Действие', $url);
//                    },
                ],
            ],
//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
