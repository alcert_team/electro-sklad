<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>

<div class="row col-12 m-0 p-0">
    <?php if (!empty($catalogs)) : ?>
        <?php foreach ($catalogs as $catalog): ?>
            <?php /** @var \app\alcert\page_elements\ProductsElement $catalog */ ?>
            <div class="col-3 pt-0 px-2 pb-2">
                <div class="col-12 m-0 p-1 text-center catalog-item">
                    <div>
                        <a href="<?= $catalog->getTitle()->getHref(); ?>"><?= $catalog->getTitle()->getText(); ?></a>
                    </div>
                    <div><img src="<?= $catalog->getImage(); ?>"/></div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
    <?php endif; ?>
</div>