<?php

/* @var $this yii\web\View */

use app\alcert\page_elements\plain\LinkElement;

$this->title = 'My Yii Application';
?>

<div class="row col-12 m-0 p-0">

    <?php
    /**
     * @var array $breadcrumbs
     * @var \app\alcert\page_elements\plain\LinkElement $breadcrumb
     */
    ?>
    <?php if ($breadcrumbs != null && count($breadcrumbs) > 0): ?>
        <div class="row mx-0 px-0 pl-3 mb-2">
            <?php $start = true; ?>
            <?php foreach ($breadcrumbs as $breadcrumb): ?>
                <?= ($start) ? '' : '<i class="pt-1 px-1 fa fa-angle-right" aria-hidden="true"></i>'; ?>
                <a href='<?= $breadcrumb->getHref(); ?>'>
                    <?= $breadcrumb->getText(); ?>
                </a>
                <?php $start = false; ?>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>

    <?php if (!empty($pages)) : ?>
        <?php
        /**
         * @var LinkElement $page
         */
        ?>
        <div class="col-12 text-right m-0 p-0 pr-2 mb-2">
            <?php foreach ($pages as $page): ?>
                <a class="page-num-btn<?= ($currentPage == $page->getText() ? "-active" : "") ?>"
                   href="<?php echo $page->getHref(); ?>"><?php echo $page->getText() ?></a>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>


    <?php if (!empty($products)) : ?>
        <div class="row col-12 m-0 p-0">
            <?php foreach ($products as $product): ?>
                <?php
                /** @var \app\alcert\page_elements\ProductsElement $product */
                ?>

                <div class="col-4 pt-0 px-2 pb-2">
                    <div class="col-12 m-0 p-1 text-center products-item">
                        <div class="mb-3">
                            <a href="<?= $product->getTitle()->getHref(); ?>"><?= $product->getTitle()->getText(); ?></a>
                        </div>

                        <div class="my-2">
                            <img src="<?= $product->getImage(); ?>"/>
                        </div>

                        <div class="font-weight-before-bold">
                            <?= empty($product->getAmount()) ?
                                "Узнать цену" : "Цена: " . $product->getAmount() . " р." ?>
                        </div>

                        <?php if (!empty($product->getOptions()) && is_array($product->getOptions())): ?>
                            <?php foreach ($product->getOptions() as $key => $option): ?>
                                <div class="small"><?= trim($key) . ": " . trim($option) ?></div>
                            <?php endforeach; ?>
                        <?php endif; ?>

                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    <?php else: ?>
    <?php endif; ?>

    <?php if (!empty($pages)) : ?>
        <?php
        /**
         * @var LinkElement $page
         */
        ?>
        <div class="col-12 text-right m-0 p-2">
            <?php foreach ($pages as $page): ?>
                <a class="page-num-btn" href="<?php echo $page->getHref(); ?>"><?php echo $page->getText() ?></a>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>