<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>

<div class="row col-12 m-0 p-0">
    <?php /** @var \app\alcert\page_elements\ProductElement $product */ ?>

    <div class="px-3">
        <?php
        /**
         * @var array $breadcrumbs
         * @var \app\alcert\page_elements\plain\LinkElement $breadcrumb
         */
        ?>
        <?php if ($breadcrumbs != null && count($breadcrumbs) > 0): ?>
            <div class="row mx-0 px-0 mb-4">
                <?php $start = true; ?>
                <?php foreach ($breadcrumbs as $breadcrumb): ?>
                    <?= ($start) ? '' : '<i class="pt-1 px-1 fa fa-angle-right" aria-hidden="true"></i>'; ?>
                    <a href='<?= $breadcrumb->getHref(); ?>'>
                        <?= $breadcrumb->getText(); ?>
                    </a>
                    <?php $start = false; ?>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>

        <div class="row mx-0 px-0 mb-4">
            <?= $product->getTitle() ?>
        </div>
        <div class="row mx-0 px-0 mb-4">
            <div class="col-3"><img src="<?= $product->getImage() ?>"/></div>
            <div class="col-9"><?= $product->getInfoTable() ?></div>
        </div>
        <div class="row mx-0 px-0 mb-4 font-weight-before-bold">
            <?= empty($product->getAmount()) ?
                "Узнать цену" : "Цена: " . $product->getAmount() . " р." ?>
        </div>
    </div>

    <?php
    $tabs = $product->getTabs();
    ?>
    <?php if (!empty($tabs)): ?>
        <div class="px-3">
            <ul id="tabsJustified" class="nav nav-tabs">
                <?php $index = 0; ?>
                <?php foreach ($tabs as $keyTab => $tab): ?>
                    <?php
                    $extClass = "";
                    if ($index == 0) {
                        $extClass = "active";
                    }
                    ?>
                    <li class="nav-item">
                        <a href=""
                           data-target="#<?= $index ?>"
                           data-toggle="tab"
                           class="nav-link small text-uppercase <?= $extClass ?>">
                            <?= $keyTab ?>
                        </a>
                    </li>
                    <?php $index++ ?>
                <?php endforeach; ?>
            </ul>

            <div id="tabsJustifiedContent" class="tab-content py-4 px-3 border-top-0 border">
                <?php $index = 0; ?>
                <?php foreach ($tabs as $keyTab => $tab): ?>
                    <?php
                    $extClass = "";
                    if ($index == 0) {
                        $extClass = "active show";
                    }
                    ?>
                    <div id="<?= $index ?>" class="tab-pane fade <?= $extClass ?>">
                        <?= $tab ?>
                    </div>
                    <?php $index++ ?>
                <?php endforeach; ?>
            </div>

        </div>
    <?php endif; ?>
</div>