<?php

namespace app\alcert\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class SearchForm extends Model
{
    public $search;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // search are required
            [['search'], 'required'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'search' => 'Поиск',
        ];
    }
}
