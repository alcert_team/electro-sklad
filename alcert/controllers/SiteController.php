<?php

namespace app\alcert\controllers;

use app\alcert\helpers\DomHelper;
use app\alcert\helpers\PageHelper;
use app\alcert\models\ContactForm;
use app\alcert\models\LoginForm;
use app\alcert\models\SearchForm;
use app\alcert\page_elements\plain\LinkElement;
use app\alcert\page_elements\ProductElement;
use simplehtmldom_1_5\simple_html_dom_node;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;


class SiteController extends Controller
{
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        /**
         * @var simple_html_dom_node $pageNameNode
         * @var ProductElement $product
         */

        $dom = DomHelper::getDomDocument();
        $dom->_charset = 'UTF-8';

        // Получаем левое меню
        $this->initSidebar($dom);
        // Получаем основную инормацию по странице
        $body = PageHelper::getBody($dom);
        // Наименование страницы
        $pageName = PageHelper::getPageName($body);

        // Получаем каталоги
        $catalogs = PageHelper::getCatalogs($body);
        if (!empty($catalogs)) {
            return $this->render('catalogs', [
                'page_name' => $pageName,
                'catalogs' => $catalogs,
            ]);
        }

        // Получаем продукты
        $products = PageHelper::getProducts($body);
        if (!empty($products)) {
            $currentPage = $this->getCurrentPage();
            $pages = PageHelper::getLinkElements($body);
            $breadcrumbs = PageHelper::getBreadcrumbs($body);
            return $this->render('products', [
                'page_name' => $pageName,
                'pages' => $pages,
                'currentPage' => $currentPage,
                'breadcrumbs' => $breadcrumbs,
                'products' => $products,
            ]);
        }

        // Получаем описание
        $product = PageHelper::getProduct($body);

        if (!empty($product)) {
            $product->setTitle($pageName);
            $breadcrumbs = PageHelper::getBreadcrumbs($body);
            return $this->render('product', [
                'page_name' => $pageName,
                'breadcrumbs' => $breadcrumbs,
                'product' => $product,
            ]);
        }

        // NOT FOUND
        throw new NotFoundHttpException("Товар не найден");
    }

    public function actionSearch()
    {
        if (Yii::$app->request->isPost) {

            // TODO поправить переход из Login в Search
            $post = Yii::$app->request->post();

            if (isset($post['LoginForm'])) {
                return $this->actionLogin();
            }

            if (isset($post['ContactForm'])) {
                return $this->actionContact();
            }
        }

        $model = new SearchForm();
        if (!$model->load(['SearchForm' => Yii::$app->request->post()])) {
            return $this->redirect(['site/index']);
        }

        /**
         * @var simple_html_dom_node $pageNameNode
         * @var ProductElement $product
         */

        $search = Yii::$app->request->post('search');

        $dom = DomHelper::getSearchDomDocument($search);
        $dom->_charset = 'UTF-8';

        // Получаем левое меню
        $this->initSidebar($dom);
        // Получаем основную инормацию по странице
        $body = PageHelper::getBody($dom);
        // Наименование страницы
        $pageName = PageHelper::getPageName($body);

        // Получаем продукты
        $products = PageHelper::getProducts($body);
        if (!empty($products)) {
            $currentPage = $this->getCurrentPage();
            $pages = PageHelper::getLinkElements($body);
            return $this->render('products', [
                'page_name' => $pageName,
                'pages' => $pages,
                'currentPage' => $currentPage,
                'products' => $products,
            ]);
        }

        throw new NotFoundHttpException("Товар не найден");
    }

    /**
     * Контакты
     * @return Response|string
     */
    public function actionContact()
    {
        $dom = DomHelper::getDomDocument("/catalog/");
        $dom->_charset = 'UTF-8';

        // Получаем левое меню
        $this->initSidebar($dom);

        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');
            return Yii::$app->getResponse()->redirect(["site/contact"]);
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * О Компании
     * @return string
     */
    public function actionAbout()
    {
        $dom = DomHelper::getDomDocument("/catalog/");
        $dom->_charset = 'UTF-8';

        // Получаем левое меню
        $this->initSidebar($dom);

        return $this->render('about');
    }

    /**
     * Доставка
     * @return string
     */
    public function actionDostavka()
    {
        $dom = DomHelper::getDomDocument("/catalog/");
        $dom->_charset = 'UTF-8';

        // Получаем левое меню
        $this->initSidebar($dom);

        return $this->render('dostavka');
    }

    /**
     * Гарантия
     * @return string
     */
    public function actionWarranty()
    {
        $dom = DomHelper::getDomDocument("/catalog/");
        $dom->_charset = 'UTF-8';

        // Получаем левое меню
        $this->initSidebar($dom);

        return $this->render('warranty');
    }

    /**
     * @return int
     */
    public function getCurrentPage()
    {
        $getParams = Yii::$app->request->getQueryParams();
        $currentPage = isset($getParams['page']) && is_numeric($getParams['page']) ? (integer)$getParams['page'] : 1;
        return $currentPage;
    }

    /**
     * @param $dom
     */
    public function initSidebar($dom)
    {
        $sidebar = PageHelper::getLeftMenuElements($dom);
        $this->getView()->params['sidebar'] = $sidebar;
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

}
