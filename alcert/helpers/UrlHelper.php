<?php

namespace app\alcert\helpers;

use Yii;

class UrlHelper
{

    public static function getLocalBaseUrl()
    {
        return sprintf(
            "%s://%s",
            isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
            $_SERVER['SERVER_NAME']
        );
    }

    public static function getAbsoluteLocalUrl()
    {
        return self::getLocalBaseUrl() . Yii::$app->request->getUrl();
    }

    public static function getRemoteBaseUrl()
    {
        return 'http://grantek-svet.ru';
    }

    public static function getAbsoluteRemoteUrl()
    {
        return self::getRemoteBaseUrl() . Yii::$app->request->getUrl();
    }

    public static function addGetParametersToUrl($url, $params)
    {
        foreach ($params as $key => $value) {
            $pattern1 = "/({$key})=.*&/i";
            $pattern2 = "/({$key})=.*/i";
            if (preg_match($pattern1, $url)) {
                $replacement = '${1}' . "=" . $value . "&";
                $url = preg_replace($pattern1, $replacement, $url);
            } elseif (preg_match($pattern2, $url)) {
                $replacement = '${1}' . "=" . $value;
                $url = preg_replace($pattern2, $replacement, $url);
            } else {
                if (strpos($url, "?") === false) {
                    $url = $url . "?{$key}={$value}";
                } else {
                    $url = $url . "&{$key}={$value}";
                }
            }
        }
        return $url;
    }

}
