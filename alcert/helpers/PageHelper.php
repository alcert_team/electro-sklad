<?php

namespace app\alcert\helpers;

use app\alcert\models\Settings;
use app\alcert\page_elements\CatalogElement;
use app\alcert\page_elements\enums\LeftMenuElementType;
use app\alcert\page_elements\LeftMenuElement;
use app\alcert\page_elements\plain\LinkElement;
use app\alcert\page_elements\plain\TextElement;
use app\alcert\page_elements\ProductElement;
use app\alcert\page_elements\ProductsElement;
use simplehtmldom_1_5\simple_html_dom;
use simplehtmldom_1_5\simple_html_dom_node;

class PageHelper
{
    public static function getBody($dom)
    {
        $body = $dom->find("#lay_body", 0);
        return $body;
    }

    public static function getPageName($body)
    {
        $pageNameNode = $body->find(".pages_name h1", 0);
        $pageName = iconv("CP1251", "UTF-8", $pageNameNode->text());
        return $pageName;
    }

    /**
     * @param simple_html_dom $node
     * @param $remoteUrl
     * @param $localUrl
     * @return array
     */
    public static function getLeftMenuElements($node)
    {
        $elements = [];

        if (DomHelper::isEmpty($node)) {
            return $elements;
        }

        $menu = $node->find(".sm_tree", 0);
        if (DomHelper::isEmpty($menu)) {
            return $elements;
        }

        $items = $menu->find(".sm_level_1,.sm_level_2");
        foreach ($items as $k => $item) {
            // Получаем элемент-ссылку
            $link = $item->find("a", 0);
            $level = $item->getAttribute("class") != "sm_level_2" ? 'level1' : 'level2';

            if (DomHelper::isNotEmpty($link)) {
                // Ссылка
                $href = str_replace(UrlHelper::getRemoteBaseUrl(), UrlHelper::getLocalBaseUrl(), $link->href);
                $text = iconv('CP1251', 'UTF-8', $link->text());

                $linkElement = new LinkElement();
                $linkElement->setHref($href);
                $linkElement->setText($text);

                $element = new LeftMenuElement();
                $element->setType(LeftMenuElementType::LINK);
                $element->setElement($linkElement);
                $element->setClass($level);

                $elements[] = $element;
                continue;
            }

            // Получаем элемент-текст
            $selectedItem = $item->find(".sm_tree_item_selected", 0);
            if (DomHelper::isNotEmpty($selectedItem)) {
                $extClass = "";
                if ($level == "level2") {
                    $extClass = " font-weight-bold";
                }

                // Выбранный элемент
                $text = iconv('CP1251', 'UTF-8', $selectedItem->text());
                $textElement = new TextElement();
                $textElement->setText($text);

                $element = new LeftMenuElement();
                $element->setType(LeftMenuElementType::TEXT);
                $element->setElement($textElement);
                $element->setClass($level . $extClass);

                $elements[] = $element;
                continue;
            }
        }
        return $elements;
    }

    /**
     * @param simple_html_dom_node $node
     * @return array
     */
    public static function getCatalogs($node)
    {
        $elements = [];
        $catalogs = $node->find(".tdCatalog");
        foreach ($catalogs as $catalog) {
            // Получаем наименование
            $titleLink = self::getTitleLinkElement($catalog);
            // Получаем картинку
            $imageSrc = self::getCatalogImageSrc($catalog);

            $element = new CatalogElement();
            $element->setTitle($titleLink);
            $element->setImage(trim($imageSrc));

            $elements[] = $element;
        }
        return $elements;
    }

    /**
     * @param simple_html_dom_node $node
     * @return array
     */
    public static function getProducts($node)
    {
        $elements = [];
        $products = $node->find(".tdProduct");
        /**
         * @var Settings $rate
         */
        $rate = (float) Settings::findByKey("rate")->value;

        foreach ($products as $product) {
            // Получаем наименование
            $titleLink = self::getTitleLinkElement($product);
            // Получаем цену
            $amountInteger = (int) str_replace(" ", "", self::getAmount($product));
            $amount = number_format($amountInteger * $rate, 0, ',', ' ');

            // Получаем картинку
            $imageSrc = self::getImageSrc($product);
            // Получаем опции
            $options = self::getOptions($product);

            $element = new ProductsElement();
            $element->setAmount($amount);
            $element->setTitle($titleLink);
            $element->setImage(trim($imageSrc));
            $element->setOptions($options);
            $elements[] = $element;
        }
        return $elements;
    }

    /**
     * @param simple_html_dom_node $node
     * @return array
     */
    public static function getBreadcrumbs($node)
    {
        $breadcrumbs = [];
        $breadcrumbsLinks = $node->find("#breadcrumb a");
        if ($breadcrumbsLinks != null && count($breadcrumbsLinks) > 1) {
            for ($i = 1; $i < count($breadcrumbsLinks); $i++) {
                $breadcrumb = new LinkElement();
                $breadcrumb->setHref(str_replace(UrlHelper::getRemoteBaseUrl(), UrlHelper::getLocalBaseUrl(), $breadcrumbsLinks[$i]->href));
                $breadcrumb->setText(iconv("CP1251", "UTF-8", $breadcrumbsLinks[$i]->text()));

                $breadcrumbs[] = $breadcrumb;
            }
        }
        return $breadcrumbs;
    }

    /**
     * @param simple_html_dom_node $node
     * @return string
     */
    public static function getProduct($node)
    {
        /**
         * @var simple_html_dom_node $blockNode
         */

        $blockNode = $node->find(".borderDiv", 0);
        /**
         * @var Settings $rate
         */
        $rate = (float) Settings::findByKey("rate")->getValue();

        // Таблица с информацией о продукте
        $infoTable = self::getInfoTable($blockNode);
        // Цена продукта
        $amountInteger = (int) str_replace(" ", "", self::getPrice($blockNode));
        $amount = number_format($amountInteger * $rate, 0, ',', ' ');

        // Вкладки продукта
        $productTabs = self::getProductTabs($node);
        // Изображение
        $imageSrc = self::getImageSrc($node);

        $element = new ProductElement();
        $element->setAmount($amount);
        $element->setImage(trim($imageSrc));
        $element->setInfoTable(trim($infoTable));
        $element->setTabs($productTabs);

        return $element;
    }

    /**
     * @param simple_html_dom_node $node
     */
    public static function getLinkElements($node)
    {
        $pagesArray = [];
        $pageBlock = $node->find(".mypagination", 0);
        if (empty($pageBlock)) {
            return $pagesArray;
        }
        $pages = $pageBlock->find("li a");
        if (!empty($pages)) {
            $lastPage = (integer)$pages[count($pages) - 1]->text();
            if ($lastPage < 2) {
                $lastPage = null;
            }
            if ($lastPage != null) {
                for ($i = 1; $i <= $lastPage; $i++) {
                    $href = UrlHelper::addGetParametersToUrl(UrlHelper::getAbsoluteLocalUrl(), ['page' => $i]);
                    $text = $i;
                    $element = new LinkElement();
                    $element->setHref($href);
                    $element->setText($text);

                    $pagesArray[] = $element;
                }
            }
        }
        return $pagesArray;
    }

    /**
     * @param simple_html_dom_node $leftItem
     */
    private static function getPrefixItemByClass($leftItem)
    {
        $prefix = "";
        $class = $leftItem->getAttribute("class");
        if ($class == "sm_level_2") {
            $prefix = "pl-3 small";
        }
        return $prefix;
    }

    /**
     * @param simple_html_dom_node $section
     * @return string
     */
    private static function getSectionValue($section)
    {
        $pattern = '/NoIndexPlz\("(.+)"\)/';
        if (preg_match($pattern, $section->outertext())) {
            $matches = [];
            preg_match_all($pattern, $section->outertext(), $matches, PREG_OFFSET_CAPTURE);
            $value = iconv("CP1251", "UTF-8", $matches[1][0][0]);
        } else {
            $value = iconv("CP1251", "UTF-8", $section->outertext());
        }
        $value = str_replace('a href="../catalog', 'a href="' . UrlHelper::getLocalBaseUrl() . '/catalog', $value);
        $value = str_replace('a href="catalog', 'a href="' . UrlHelper::getLocalBaseUrl() . '/catalog', $value);
        $value = str_replace('img src="../img', 'img src="' . UrlHelper::getRemoteBaseUrl() . '/img', $value);
        $value = str_replace('img src="/img', 'img src="' . UrlHelper::getRemoteBaseUrl() . '/img', $value);
        return $value;
    }

    /**
     * @param simple_html_dom_node $node
     * @return string
     */
    public static function getImageSrc($node)
    {
        $image = $node->find(".img_product img", 0);
        return UrlHelper::getRemoteBaseUrl() . $image->src;
    }

    /**
     * @param simple_html_dom_node $node
     * @return string
     */
    public static function getCatalogImageSrc($node)
    {
        $image = $node->find("a img", 0);
        return UrlHelper::getRemoteBaseUrl() . str_replace("../", "/", $image->src);
    }

    /**
     * @param simple_html_dom_node $node
     * @return LinkElement
     */
    public static function getTitleLinkElement($node)
    {
        $title = $node->find(".title a", 0);
        $titleLink = new LinkElement();
        $titleLink->setText(iconv('CP1251', 'UTF-8', trim($title->text())));
        $titleLink->setHref(str_replace(UrlHelper::getRemoteBaseUrl(), UrlHelper::getLocalBaseUrl(), $title->href));
        return $titleLink;
    }

    /**
     * @param simple_html_dom_node $node
     * @return string
     */
    public static function getAmount($node)
    {
        $amount = $node->find(".viewCena span", 0);
        $amountText = null;
        if ($amount != null) {
            $amountText = iconv('CP1251', 'UTF-8', $amount->text());
        }
        return $amountText;
    }

    /**
     * @param simple_html_dom_node $node
     * @return string
     */
    public static function getPrice($node)
    {
        $amount = $node->find('.viewCena_page1 span[itemprop="price"]', 0);
        $amountText = null;
        if ($amount != null) {
            $amountText = iconv('CP1251', 'UTF-8', $amount->text());
        }
        return $amountText;
    }

    /**
     * @param simple_html_dom_node $node
     * @return array
     */
    public static function getOptions($node)
    {
        $optionsArray = [];
        $options = $node->find(".option_block .opt");
        foreach ($options as $option) {
            $key = $option->find(".op_name", 0);
            $value = $option->find(".op_value", 0);
            $optionsArray[iconv('CP1251', 'UTF-8', $key->text())] = iconv('CP1251', 'UTF-8', $value->text());
        }
        return $optionsArray;
    }

    /**
     * @param simple_html_dom_node $node
     * @return array
     */
    public static function getProductTabs($node)
    {
        $tabs = $node->find(".tabs", 0);
        if (empty($tabs)) {
            return [];
        }
        $sections = $tabs->find("section");
        if (empty($sections)) {
            return [];
        }

        $productTabs = [];
        foreach ($sections as $section) {
            $sectionId = $section->id;
            $id = str_replace("content", "", $sectionId);
            $label = $tabs->find('label[for=tab' . $id . ']', 0);
            $key = trim(iconv("CP1251", "UTF-8", $label->text()));

            // Фильтр ненужных вкладок
            if (mb_strtoupper($key, 'UTF-8') == "ФОТО" || mb_strtoupper($key, 'UTF-8') == "СЕРТИФИКАТЫ") continue;

            $value = self::getSectionValue($section);

            $productTabs[$key] = str_replace(array("<br>", "<br/>", "<br />", "<p>&nbsp;</p>"), "", $value);
        }
        return $productTabs;
    }

    /**
     * @param simple_html_dom_node $node
     * @return string
     */
    public static function getInfoTable($node)
    {
        $infoTable = $node->find("table[class=viewTable]", 0);
        $infoTable = iconv("CP1251", "UTF-8", $infoTable->outertext());
        return $infoTable;
    }
}