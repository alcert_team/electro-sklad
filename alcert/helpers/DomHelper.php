<?php

namespace app\alcert\helpers;

use app\alcert\exceptions\MaxFileSizeException;
use simplehtmldom_1_5\simple_html_dom;
use simplehtmldom_1_5\simple_html_dom_node;
use Sunra\PhpSimple\HtmlDomParser;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

class DomHelper
{
    public static function getDomDocument($url = null)
    {
        if (is_null($url)) {
            $url = Yii::$app->request->getUrl();
            if ($url == "/") {
                $url = "/catalog/";
            }
        }

        $cache = Yii::$app->cache;
        $contents = $cache->get($url);
        if ($contents === false) {
            $remoteBaseUrl = UrlHelper::getRemoteBaseUrl();
            $remoteAbsoluteUrl = $remoteBaseUrl . $url;
            /**
             * @var \GuzzleHttp\Client $client
             * @var \GuzzleHttp\Psr7\Response $response
             * @var \GuzzleHttp\Psr7\Stream $body
             */
            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', $remoteAbsoluteUrl);
            $body = $response->getBody();
            $contents = $body->getContents();

            $cache->set($url, $contents, 300);
        }
        return HtmlDomParser::str_get_html($contents);
    }

    public static function getSearchDomDocument($search)
    {
        $remoteAbsoluteUrl = UrlHelper::getRemoteBaseUrl() . "/modules/search/index.php";
        /**
         * @var \GuzzleHttp\Client $client
         * @var \GuzzleHttp\Psr7\Response $response
         * @var \GuzzleHttp\Psr7\Stream $body
         */

        $search = iconv("UTF-8", "windows-1251", $search);

        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', $remoteAbsoluteUrl, [
            'form_params' => [
                'search' => $search,
                'x' => '0',
                'y' => '0',
            ]
        ]);

        $body = $response->getBody();
        $contents = $body->getContents();

        define('MAX_FILE_SIZE', 5000000);
        if (empty($contents) || strlen($contents) > MAX_FILE_SIZE) {
            throw new MaxFileSizeException("Найдено слишком много элементов. Пожалуйста,скорректируйте запрос.");
        }
        return HtmlDomParser::str_get_html($contents);
    }

    /**
     * @param simple_html_dom_node | simple_html_dom $node
     * @return boolean
     */
    public static function isEmpty($node)
    {
        return $node == null || $node == false;
    }

    /**
     * @param simple_html_dom_node | simple_html_dom $node
     * @return boolean
     */
    public static function isNotEmpty($node)
    {
        return $node != null && $node != false;
    }
}