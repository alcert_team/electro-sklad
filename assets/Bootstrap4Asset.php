<?php

namespace app\assets;

use yii\web\AssetBundle;

class Bootstrap4Asset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap/bootstrap.min.css',
        'css/bootstrap/bootstrap.min.css.map',
        'font-awesome/css/font-awesome.css',
        'css/custom-bootstrap.css',
    ];
    public $js = [
//        'js/jquery.min.js',
        'js/tether.min.js',
        'js/bootstrap.min.js',
        'js/functions.js'
    ];
}
